import numpy as np
import matplotlib.pyplot as plt
import re
d = {'prometheus-federator':[],'prometheus':[],'prometheus2':[]}

with open("log.txt") as f:
    content = f.readlines()

for x in content:
    matchObj = re.match( r'.*m.*_(.*)-data.*\D{3}(.*)MB', x)
    if matchObj is not None:
        d[matchObj.group(1)].append(float(matchObj.group(2)))
    
fig, ax = plt.subplots()
for key in d:
    line1, = ax.plot(np.array(range(len(d[key])))/(6*60), np.array(d[key]),
                 label=key)

plt.title("Disk usage over time")
plt.xlabel('time (hours)')
plt.ylabel('MB')
ax.legend(loc='center right')
plt.show()