# Prometheus High Availability

## introduction

Prometheus is one of the most popular open source monitoring systems nowadays and it has a growing ecosystem which develops many integrations and improve its performance to make it even better!

It's very easy to set up Prometheus, especially with docker. However, when it comes to High Availability configuration, things become tricky.

In the following article I will create a basic dockerized monitoring stack and apply some tricks to make it highly available.

## Background

Before I will go through the configurations, I would like to give some intuition for the trick. 

We can represent the data in time series database like Prometheus as the following:

![time1](images/time1.png)

The horizontal axis represents how the data is distributed over the time and the vertical axis represents the time series. Each time series has name and a set of labels. Each time series has a unique set of labels!

For example, the first 2 time series share the same name but have different values for the "method" label. 

Another important concept is called *federation*. It is defined on [prometheous' documentation](https://prometheus.io/docs/prometheus/latest/federation/) as:

*"Federation allows a Prometheus server to scrape selected time series from another Prometheus server."* 

Common use case is to take advantage of this mechanism to aggregate data from different Prometheus instances which monitor different environments. It's referred to as *Hierarchical federation*.    

If you federate Prometheus instance, all the labels which are configured under ``` external_labels ``` (on Prometheus' configuration) are added to every time series which come from this instance. Let's say we configured an external label which is called *"monitor"*  with value *prom* for the instance we are federating, we will get the following:

![time2](images/time2.png)

As you can see, the external label was added to the time series which came from this instance. If we federate another instance with the value *prom2* we will get the following:

![time3](images/time3.png)


It doubled the number of time-series because each federated time series has a "monitor" label for each source. This was helpful if each Prometheus instance monitored different environment like in *Hierarchical federation* where you want to know the source of each time series.

So what about ignoring the external labels and monitoring the same environment with two different Prometheus instances? **We will achieve high availability!**

The time series which will come from the different instances will have the same set of labels, so the scrapes will merge into the same time series. 

It will looks like this:

![time4](images/time4.png)

The federated time series have twice scrapes as the original time series!

 Now let's see how it will look like if one instance will be down for a while:

![time5](images/time5.png)

As you have probably guessed, the dense area is where both instances were up and the sparse area represents the time which one instance was down.

### Docker Stack

After we understood the theory, lets implement it!

The docker stack includes the following services:

- Prometheus *(port 9090)*

-  Prometheus 2 *(port 9092)*

- Prometheus-federator *(port 9095)*

- Alert Manager *(port 9093)*

- Alert Manager2 *(port 9094)*

- Grafana *(port 3000)*

- Node-exporter

  ![diagram](images/diagram.png)

  This diagram represents the relations between the different components. Each color represents different kind of relation:

  **blue** - Scraping data from the node exporter.

  **orange** - Connections with the alert managers. The connection between the alert managers represents their high-availability. 

  **green** - The federation process - the federator receives all the data that ingested by the other instances. 

  **black** - Grafana queries the federator instance for plotting graphs.

  #### Prometheus

  Prometheus is the time series database. Its service configuration on the docker-compose file looks like this:

  ```yaml
    prometheus:
      image: prom/prometheus:v2.0.0
      ports: 
        - 9091:9090
      volumes:
      - type: volume
        source: prometheus-data
        target: /prometheus
      configs:
      - source: prometheus.yml
        target: /etc/prometheus/prometheus.yml
      command:
      - --config.file=/etc/prometheus/prometheus.yml
  ```

  -  *ports*:  By default, Prometheus' web interface is exposed on port 9090. Since we set up 3 Prometheus instances on the same host, we map each container to a different port on the host.
  -  *volumes*: To create isolation between the data of each Prometheus instance, we mount every instance to a different docker volume.
  -  *configs*: We use docker configs mechanism to store the configurations of Prometheus. 
  -  *command*: We add this flag to tell Prometheus which configuration file to use.

  #### Alertmanager

  Alertmanager is the component that handles the alerts which are reported from the Prometheus instances. We will use two alertmanagers to achieve high-availability. Its service configuration on the docker-compose file looks like this:   

  ```yaml
    alertmanager:
      image: prom/alertmanager:v0.9.1
      hostname: '{{.Node.Hostname}}'
      ports: 
        - 9093:9093
      configs:
      - source: alertmanager.yml
        target: /etc/alertmanager/alertmanager.yml
      command:
      - -config.file=/etc/alertmanager/alertmanager.yml
      - --mesh.peer-id=00:00:00:00:00:01 
      - --mesh.nickname=a
      - --log.level=debug
    
     
    alertmanager2:
      image: prom/alertmanager:v0.9.1
      hostname: '{{.Node.Hostname}}'
      ports: 
        - 9094:9093
      configs:
      - source: alertmanager.yml
        target: /etc/alertmanager/alertmanager.yml
      command:
      - -config.file=/etc/alertmanager/alertmanager.yml
      - --mesh.peer-id=00:00:00:00:00:02
      - --mesh.nickname=b
      - --mesh.peer=alertmanager:6783
      - --log.level=debug
  ```

  The above flags are taken from the [Procfile](https://github.com/prometheus/alertmanager/blob/master/Procfile) in the Alert Manager's official repository.

  The following are some notes:

   - The ```mesh.peer-id``` should be unique. By default it's the *mac-address*, so we override it to make it more human-friendly . 

   - The ```nickname``` shouldn't be unique, so by default it's the host name. If we run it on *localhost*, their nicknames will be identical so we override it.   	

  - On alertmanager2 we mapped the default port (9093) to port 9094 on the host machine. 

  - On alert alertmanager2 we also set ```mesh.peer``` with the service name *alertmanager* and the default listening port.  

    For more details you can see the full documentation [here](https://github.com/prometheus/alertmanager#high-availability).

  If everything set correctly, after you deploy the stack,  you should see something like this when you open your browser on: ```http://127.0.0.1:9093/#/status``` 

  ![alertmanager mesh status](images/Alertmanager.png)

  ​

  ​

#### Putting It All Together

Since we want to keep it simple, we will use the default configuration for the *node exporter*. Also, will add *Grafana* service for visualization. 

[docker-compose.yml](assets/docker-compose.yml)

You can deploy this stack by typing:

``` bash
docker stack deploy -c docker-compose.yml monitoring
```



### Service Configurations 

These files are the applicative configurations of each service. They should be saved where they are referenced in the *configs* section. 

#### Prometheus Configuration

This is the configuration for Prometheus1 and Prometheus2. Their configurations are almost Identical except for the ```external_labels```. Each Prometheus instance should have a unique set of external labels.  In our example we will set an external label called "monitor". For Prometheus1 we will set it's value to "prom" and for Prometheus2 we will set it to "prom2". 

The external labels are added to any time series or alerts when communicating with external systems. Later we will take advantage on this fact to create high availability.

Some notes about the other configurations:

- Prometheous will identify each file which ends with: ".rules.yml" as rule file. We use regular expression instead of listing them manually
- In the ```scarpe_configs``` section,  we configure only one job for our node-exporter. *tasks.node-exporter*  returns the addresses of all node exporters in the swarm.
- In the alerting section we configure manually two alertmanagers on ```localhost``` and their corresponding ports.   





prometheus.yml:

```yaml
global:
  scrape_interval:     15s
  evaluation_interval: 15s

  external_labels:
    monitor: 'prom'

rule_files:
  - "*.rules.yml"

scrape_configs:

  - job_name: 'node-exporter'
    dns_sd_configs:
    - names:
      - 'tasks.node-exporter'
      type: 'A'
      port: 9100

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - 0.0.0.0:9093
      - 0.0.0.0:9094

```

#### Prometheus Federator Configuration

Here all the **magic** happens!

First we declare a job called  'federate'. 

Inside this job we configure the logic as described in the background section:

``` yaml 
  - job_name: 'federate'
    scrape_interval: 15s
    metric_relabel_configs:
    - regex: 'monitor'
      action: labeldrop
```

We delete the ```monitor``` label for each time series which come from the "federate" job!

prometheus-federation.yml:

``` yaml
global:
  scrape_interval:     15s
  evaluation_interval: 15s

  external_labels:
    monitor: 'federator'

rule_files:
  - "*.rules.yml"

scrape_configs:

  - job_name: 'federate'
    scrape_interval: 15s
    metric_relabel_configs:
    - regex: 'monitor'
      action: labeldrop
    dns_sd_configs:
    - names:
      - 'tasks.prometheus'
      - 'tasks.prometheus2'
      type: 'A'
      port: 9090

    honor_labels: true
    metrics_path: '/federate'

    params:
      'match[]':
        - '{job="node-exporter"}'

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - 0.0.0.0:9093
      - 0.0.0.0:9094

```

## Demo

To demonstrate our high-availability configuration, we used the following steps:

- Deploying our stack

- Creating a dashboard in grafana which monitor system's cpu. 

- Configuring 3 data sources  for Grafana: prom1, prom2 and federator.  

  Since all Prometheus instances are monitoring the same node, their plots overlap  

- Removing *prom2* service for a few minutes

- Deploying our stack again

  This is how it's look like in Grafana:   

![demo](images/demo.png)

As you can see, the graph that came from the federator is almost identical to the graph which came from *prom1* which was up all the time!  

So the downtime of *prom2* didn't affect the federator graph!

## Disk Usage

It's clear that *there ain't no such thing as a free lunch* - using more components requires more resources. Since the trick that is presented above has to do with setting up federation Prometheus instance, the question is, what is the disk usage of this instance? Does it consume twice disk usage as a regular Prometheus instance?  To put it shortly, *No.* 

To check this issue, I set a cron job which ran the following command for 3 days:

``` bash
docker system df -v | grep  "monitoring_prometheus" | tail -3  >> log.txt
```

 This command gave an output like this:

```
monitoring_prometheus-data                                         1                   268.4MB
monitoring_prometheus2-data                                        1                   268.4MB
monitoring_prometheus-federator-data                               1                   268.4MB
```

Then I used python to parse this output and created the following graph:

![disk usage](images/disk.png)

The good news here is that the federator's storage grows approximately linearly with the other instances disk usage. 

*You may ask yourself about the "steps" in the plots. They are related to the fact that Prometheus groups samples into blocks of two hours as described on the [official documentation](https://prometheus.io/docs/prometheus/latest/storage/).*

You shouldn't worry about the fact that the graph keeps rising, since we can set the following flag:

`--storage.tsdb.retention  ` on the docker-compose, which  determines when to remove old data. The following graph will demonstrate a strategy which removes old data on the federator every 4 hours and  from the other instances every 2 hours.

*Note that since the federator is the only instance which is connected to Grafana, we don't really care how much time the data will be saved on the other instances, since it will be stored on the federator anyway and Grafana will query it.*

![disk2](images/disk2.png)

 

As expected, after about 7 hours the **graph stablized!**

# Summary

We have demonstrated how to use the federation feature which comes out-of-the-box with Prometheus to achieve high availability. We also gave an example for using Alert Manager's high availability feature. 

From this point *the sky's the limit*, you can add different types of exporters, scale it up and customize this stack to your needs! 